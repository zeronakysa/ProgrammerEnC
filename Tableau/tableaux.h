#include <stdio.h>
#include <stdlib.h>

void ordonnerTableau(int tableau[], int tailleTableau);
void copieTableau(int tableauOriginal[], int tableauCopie[], int tailleTableau);
void maximumTableau(int tableau[], int tailleTableau, int valeurMax);
double moyenneTableau(int tableau[], int tailleTableau);
int sommeTableau(int tableau[], int tailleTableau);
