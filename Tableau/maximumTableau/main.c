#include <stdio.h>
#include <stdlib.h>

// Prototype de la fonction d'affichage
void maximumTableau(int tableau[], int tailleTableau, int valeurMax);


int main(int argc, char *argv[])
{
    int tableau[4] = {10, 15, 3};

    // On affiche le contenu du tableau
    maximumTableau(tableau, 4, 11);

    return 0;
}

void maximumTableau(int tableau[], int tailleTableau, int valeurMax)
{
    int i;
    for (i = 0 ; i < tailleTableau ; i++)
    {
        if (tableau[i] > valeurMax)
            tableau[i]=0;
        printf(" %d\n", tableau[i]);
    }
}
