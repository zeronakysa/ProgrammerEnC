#include <stdio.h>
#include <stdlib.h>

void ordonnerTableau(int tableau[], int tailleTableau);


int main(int argc, char *argv[])
{
    int tableau[11] = {58, 81, 42, 104, 12, 96, 75, 22, 45, 78, 36, 21};

    ordonnerTableau(tableau, 12);

    return 0;
}

void ordonnerTableau(int tableau[], int tailleTableau)
{

    int i, j, k = 0;
    for (i=0 ; i < tailleTableau ; i++)
    {
        for (j=i ; j < tailleTableau ; j++)
        {
            if (tableau[i] < tableau[j])
            {
                k = tableau[j];
                tableau[j] = tableau[i];
                tableau[i] = k;
            }
        }
        printf("%d\n", tableau[i]);
    }
    /*
    i=0;
    do
    {
        printf("%d. %d\n", i, tableau[i]);
        i++;
    }

    while (i != tailleTableau);
    */
}
