#include <stdio.h>
#include <stdlib.h>
#include "tableaux.h"

void ordonnerTableau(int tableau[], int tailleTableau)
{
    int i, j, k = 0;

    for (i=0 ; i < tailleTableau ; i++)
    {
        for (j=i ; j < tailleTableau ; j++)
        {
            if (tableau[i] < tableau[j])
            {
                k = tableau[j];
                tableau[j] = tableau[i];
                tableau[i] = k;
            }
        }
        printf("%d\n", tableau[i]);
    }
}

void copieTableau(int tableauOriginal[], int tableauCopie[], int tailleTableau)
{
    int i;

    printf("Phase 1: \n");
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("Tableau O case %d: %d\n", i, tableauOriginal[i]);
        printf("Tableau C case %d: %d\n", i, tableauCopie[i]);

    }
    printf("Phase 2: \n");
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("Tableau O case %d: %d\n", i, tableauOriginal[i]);
        tableauCopie[i] = tableauOriginal[i];
        printf("Tableau C case %d: %d\n", i, tableauCopie[i]);

    }
}

void maximumTableau(int tableau[], int tailleTableau, int valeurMax)
{
    int i;
    for (i = 0 ; i < tailleTableau ; i++)
    {
        if (tableau[i] > valeurMax)
            tableau[i]=0;
        printf(" %d\n", tableau[i]);
    }
}

double moyenneTableau(int tableau[], int tailleTableau)
{
    int i;
    double resultat = 0;
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf(" %d\n", tableau[i]);
        resultat += tableau[i];
    }
    if (i==tailleTableau)
        resultat=resultat/i;
        printf("\nResultat: %f\n", resultat);
}

int sommeTableau(int tableau[], int tailleTableau)
{
    int i, resultat = 0;
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf(" %d\n", tableau[i]);
        resultat += tableau[i];
    }
    if (i==tailleTableau)
        printf("\nResultat: %d\n", resultat);
}
