#include <stdio.h>
#include <stdlib.h>

// Prototype de la fonction d'affichage
void copie(int tableauOriginal[], int tableauCopie[], int tailleTableau);


int main(int argc, char *argv[])
{
    int tableauOriginal[4] = {10, 15, 3};
    int tableauCopie[4] = {0};

    // On affiche le contenu du tableau
    copie(tableauOriginal, tableauCopie, 4);

    return 0;
}

void copie(int tableauOriginal[], int tableauCopie[], int tailleTableau)
{
    int i;

    printf("Phase 1: \n");
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("Tableau O case %d: %d\n", i, tableauOriginal[i]);
        printf("Tableau C case %d: %d\n", i, tableauCopie[i]);

    }
    printf("Phase 2: \n");
    for (i = 0 ; i < tailleTableau ; i++)
    {
        printf("Tableau O case %d: %d\n", i, tableauOriginal[i]);
        tableauCopie[i] = tableauOriginal[i];
        printf("Tableau C case %d: %d\n", i, tableauCopie[i]);

    }
}
