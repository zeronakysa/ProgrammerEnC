#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{                                                  /* Initialisation des constantes et autres variables. */
    int niveau = 1;
    int quantiteOr = 100;
    int attaque = 15 + niveau;
    int ameliorationATK = 1, ameliorationOr = 1;
    int coutAmeliorationAtk = 100, coutAmeliorationOr = 100;
    int premierLancement = 0;                     /*Mettre en 1 pour voir les regles au premier lancement.*/
    int etatDuJeu = 2, debugMode = 2;
    int difficulte = 2, hardcoreMode = 0;
    int nombreEntre = 0
    int monstreTuer = 0;
    int nombreSRAND = 1;
    const int facilMAX = 50, normalMAX = 100, difficilMAX = 1000, impossibleMAX = 10000;
    const int facilMIN = 1, normalMIN = 1, difficilMIN = 100, impossibleMIN = 1000;

    while (etatDuJeu >= 1)                       /*Etat du jeu si eteint la fenetre se ferme + Reinitialisation des choix.*/
    {
        int choixMenu = 0;
        int choixLancement = 0;
        int choixConfirmation = 0;
        int choixAutre = 0;

        srand(time(NULL));                       /*Attribution du nombre aleatoire en fonction de la difficulte*/

        if (difficulte == 1)
            nombreSRAND = (rand() % (facilMAX - facilMIN + 1)) + facilMIN;
        if (difficulte == 2)
            nombreSRAND = (rand() % (normalMAX - normalMIN + 1)) + normalMIN;
        if (difficulte == 3)
            nombreSRAND = (rand() % (difficilMAX - difficilMIN + 1)) + difficilMIN;
        if (difficulte == 4)
            nombreSRAND = (rand() % (impossibleMAX - impossibleMIN + 1)) + impossibleMIN;


        if (debugMode == 45) //DebugMod avec visu des variable et autres.
        {
            printf(" ** Debug mode active ** \n ** Verification des variables ** \n");
            printf(" ** niveau: %d | quantiteOr: %d | attaque: %d | premierLancement: %d **\n",niveau, quantiteOr, attaque, premierLancement);
            printf(" ** amelioATK: %d | coutAmelioATK: %d | amelioOr: %d | coutAmelioOr: %d **\n",ameliorationATK, coutAmeliorationAtk, ameliorationOr, coutAmeliorationOr);
            printf(" ** aleatoire: %d | difficulte: %d | hardcore: %d ** \n",nombreSRAND,difficulte,hardcoreMode);
            printf(" ** 1001/1002/1003 pour modifier le niveau/l'or/l'attaque **\n");
        }
        while (premierLancement == 1)// Si c'est le premier lancement, ont affiche les regles une premiere fois.
        {
            printf("\n===Regles===\n\n");
            printf("Le but est d'aller au plus haut niveau possible.\n");
            printf("Il existe quatre mode de difficulte: facile, normal, difficile et impossible.\n");
            printf("Chaque niveau contient un nombre aleatoire de monstre.\n");
            printf("Le nombre de monstre depend de la difficulte. (Le nombre reste aleatoire mais est plus grand)\n");
            printf("Pour passer un niveau il te faut, au moins, le meme nombre d'attaque qu'il y a de monstres.\n");
            printf("Chaque fois que tu tue un monstre, tu gagne de l'or. Chaque fois que tu passe un niveau aussi.\n");
            printf("Tu peux ameliorer ton attaque et la quantite d'or gagner avant ou apres chaque niveau. \n");
            printf("Plus tu ameliore ton attaque plus tu iras loin en niveau, plus tu gagneras d'or.\n");
            printf("Voila pour les regles de mon jeu, elle sont simple mais efficace. \n\n As-tu bien compris ?\n");
            printf(" 1. Oui.\n 2. Non.\n");
            scanf("%d", &choixLancement);
            switch (choixLancement)
            {
                case 1:
                    system("cls");
                    premierLancement = 0;
                    break;
                case 2:
                    system("cls");
                    premierLancement = 1;
                    break;
                default:
                    system("cls");
                    premierLancement = 1;
                    printf("Tu ne peux choisir que 1 (Oui) ou 2 (Non).\nEssaye encore.\n\n");
                    break;
            }
        }
    //Stats + Niveau etc
    printf("\n===Statistiques===\n\n");
    printf(" Difficulte : ");
    if (difficulte == 1)
        printf("Facile\n");
    if (difficulte == 2)
        printf("Normal\n");
    if (difficulte == 3)
        printf("Difficile\n");
    if (difficulte == 4)
        printf("Impossible\n");
    printf(" Mode Hardcore : ");
    if (hardcoreMode == 0)
        printf("Desactive\n\n");
    if (hardcoreMode == 1)
        printf("Active\n\n");
    printf(" Attaque : %d\n", attaque);
    printf(" Niveau : %d\n", niveau);
    printf(" Or : %d\n\n", quantiteOr);
    printf(" Monstre tuer: %d\n\n", monstreTuer);


    //Message de bienvenue et de choix.
    printf("=== Menu === \n\nBonjour jeune aventurier, bienvenue dans mon petit jeu.\n\n");
    printf("Que veux-tu faire ?\n\n");
    printf(" 1. Ameliorer mon attaque (Cout: %d d'or).\n", coutAmeliorationAtk);
    printf(" 2. Ameliorer mon gain d'or (Cout: %d d'or).\n", coutAmeliorationOr);
    printf(" 3. Changer le niveau de difficulte.\n");
    printf(" 4. Explorer le niveau suivant ! (Niveau %d)\n", niveau+1);
    printf(" 5. Rappel moi les regles du jeu !\n");
    printf(" 6. Je veux quitter le jeu.\n");

    scanf("%d", &choixMenu);
        //Gros menu en forme de pavet
    switch (choixMenu)
    {
    case 1:
        system("cls");
        if (quantiteOr >= coutAmeliorationAtk)
        {
            ameliorationATK++;
            attaque++;
            quantiteOr = quantiteOr - coutAmeliorationAtk;
            coutAmeliorationAtk = coutAmeliorationAtk + 10;
            printf("Amelioration reussi avec succes.\n\n");
        }
        else
            printf("Tu n'as pas assez d'or pour ameliorer ton attaque. Retourne farmer.\n\n");
        break;
    case 2:
        system("cls");
        if (quantiteOr >= coutAmeliorationOr)
        {
            ameliorationOr++;
            quantiteOr = quantiteOr - coutAmeliorationAtk;
            coutAmeliorationOr = coutAmeliorationOr + 10;
            printf("Amelioration reussi avec succes.\n\n");
        }
        else
            printf("Tu n'as pas assez d'or pour ameliorer ton revenue d'or. Retourne farmer.\n\n");
        break;
    case 3: // Changer le niveau de difficulte
        swiftDifficulty(difficulte);
   /*     system("cls");
        printf("\n ~~ Work In Progress ~~ \n\n");
        printf(" Le niveau de difficulte actuel est ");
        if (difficulte == 1)
            printf("facile.\n");
        if (difficulte == 2)
            printf("normal.\n");
        if (difficulte == 3)
            printf("difficile.\n");
        if (difficulte == 4)
            printf("impossible.\n");
        printf("\n Quel est le niveau de difficulte que tu souhaite ?\n\n");
        printf(" 1. Facile.\n");
        printf(" 2. Normale.\n");
        printf(" 3. Difficile.\n");
        printf(" 4. Impossible.\n");
        scanf("%d", &choixAutre);
            switch (choixAutre) // Choix du niveau de difficultee
            {
                case 1:
                    system("cls");
                    difficulte = 1;
                    printf(" La difficulte facile a ete apliquer avec succes\n\n");
                    break;
                case 2:
                    system("cls");
                    difficulte = 2;
                    printf(" La difficulte normale a ete apliquer avec succes\n\n");
                    break;
                case 3:
                    system("cls");
                    difficulte = 3;
                    printf(" La difficulte difficile a ete apliquer avec succes\n\n");
                    break;
                case 4:
                    system("cls");
                    difficulte = 4;
                    printf(" La difficulte impossible a ete apliquer avec succes\n\n");
                    break;
                default:
                    system("cls");
                    printf("Mauvaise reponse, retrour au menu. Essaye avec 1, 2, 3 ou 4.\n\n");
                    break;
            }*/
            //system("cls");
        break;
    case 4:
        system("cls");
        printf("\n ~~ Work In Progress ~~ \n\n");
        do
        {
            compteurCoups++;
            printf("Quel est le nombre ? ");
            scanf("%d", &nombreEntre);
            if (nombreSRAND > nombreEntre)
                printf("C'est plus !\n\n");
            else if (nombreSRAND < nombreEntre)
                printf("C'est moins !\n\n");
            else
                printf (" Bravo, vous avez trouve le nombre myst�re en %d coups.\n\n", compteurCoups);
        } while (nombreEntre != nombreSRAND);
        break;
    case 5:
        system("cls");
        premierLancement = 1;
        break;
    case 6:
        system("cls");
        etatDuJeu--;
        printf("Es-tu sur de toi ?\n Toute progression sera perdu(je sais pas encore faire de sauvegarde).\n");
        printf(" 1. Oui, je veux quitter.\n");
        printf(" 2. Non j'ai missclick ou misstap en l'occurence.\n");
        printf("Toute autre reponse sera consideree comme un non.\n");
        scanf("%d",&choixConfirmation);
        switch (choixConfirmation)
            {
            case 1:
                system("cls");
                etatDuJeu--;
                break;
            default:
                system("cls");
                etatDuJeu = 2;
                break;
            }
        break;
    case 3357:
        system("cls");
        if (debugMode == 45)
            debugMode = 2;
        else
            debugMode = 45;
        break;
    default:
        system("cls");
        printf("Tu ne peux choisir que entre le menu 1, 2, 3, 4 , 5 ou 6. Reessaye.\n");
        break;
    }
    }
  return 0;
}


int swiftDifficulty(int difficulte)
{
    system("cls");
        printf("\n ~~ Work In Progress ~~ \n\n");
        printf(" Le niveau de difficulte actuel est ");
        if (difficulte == 1)
            printf("facile.\n");
        if (difficulte == 2)
            printf("normal.\n");
        if (difficulte == 3)
            printf("difficile.\n");
        if (difficulte == 4)
            printf("impossible.\n");
        printf("\n Quel est le niveau de difficulte que tu souhaite ?\n\n");
        printf(" 1. Facile.\n");
        printf(" 2. Normale.\n");
        printf(" 3. Difficile.\n");
        printf(" 4. Impossible.\n");
        scanf("%d", &choixAutre);
            switch (choixAutre) // Choix du niveau de difficultee
            {
                case 1:
                    system("cls");
                    difficulte = 1;
                    printf(" La difficulte facile a ete apliquer avec succes\n\n");
                    break;
                case 2:
                    system("cls");
                    difficulte = 2;
                    printf(" La difficulte normale a ete apliquer avec succes\n\n");
                    break;
                case 3:
                    system("cls");
                    difficulte = 3;
                    printf(" La difficulte difficile a ete apliquer avec succes\n\n");
                    break;
                case 4:
                    system("cls");
                    difficulte = 4;
                    printf(" La difficulte impossible a ete apliquer avec succes\n\n");
                    break;
                default:
                    system("cls");
                    printf("Mauvaise reponse, retrour au menu. Essaye avec 1, 2, 3 ou 4.\n\n");
                    break;
            }
            return (difficulte)
}
