#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    int reset = 2;

    do                                              /* Reset possible si reset == 1 */
       {
                                                    /* Initialisation des constantes et autres variables. */
    int niveau = 0;
    int quantiteOr = 100;
    int coups = 15 + niveau;
    int ameliorationCoups = 0, ameliorationOr = 0;
    int coutAmeliorationCoups = 100, coutAmeliorationOr = 100;
    int premierLancement = 1;                      /*Mettre en 1 pour voir les regles au premier lancement.*/
    int etatDuJeu = 2, debugMode = 2;
    int difficulte = 2, hardcoreMode = 0;
    int nombreEntre = 0, compteurCoups = 0;
    int essaieRealiser = 0, victoire = 0, defaite = 0;
    int nombreSRAND = 1;
    const int facilMAX = 50, normalMAX = 100, difficilMAX = 1000, impossibleMAX = 10000;
    const int facilMIN = 1, normalMIN = 1, difficilMIN = 1, impossibleMIN = 1;

    while (etatDuJeu >= 1)                       /*Etat du jeu si eteint la fenetre se ferme + Reinitialisation des choix.*/
    {
        int choixMenu = 0, choixLancement = 0, choixConfirmation = 0;
        int coupsRestants = 10 + ameliorationCoups;

        srand(time(NULL));                      /*Attribution du nombre aleatoire en fonction de la difficulte*/
        reset = 0;

        if (difficulte == 1)
            nombreSRAND = (rand() % ((facilMAX+niveau) - facilMIN + 1)) + facilMIN;
        if (difficulte == 2)
            nombreSRAND = (rand() % ((normalMAX+niveau) - normalMIN + 1)) + normalMIN;
        if (difficulte == 3)
            nombreSRAND = (rand() % ((difficilMAX+niveau) - difficilMIN + 1)) + difficilMIN;
        if (difficulte == 4)
            nombreSRAND = (rand() % ((impossibleMAX+niveau) - impossibleMIN + 1)) + impossibleMIN;

        if (debugMode == 45) //DebugMod avec visu des variable et autres.
        {
            printf("\n ** Debug mode active ** \n ** Verification des variables ** \n");
            printf(" ** niveau: %d | quantiteOr: %d | coups: %d | premierLancement: %d **\n",niveau, quantiteOr, coups, premierLancement);
            printf(" ** amelioCoups: %d | coutAmelioCoups: %d | amelioOr: %d | coutAmelioOr: %d **\n",ameliorationCoups, coutAmeliorationCoups, ameliorationOr, coutAmeliorationOr);
            printf(" ** aleatoire: %d | difficulte: %d | hardcore: %d ** \n",nombreSRAND,difficulte,hardcoreMode);
            printf(" ** 1001/1002/1003 pour modifier le niveau/l'or/le nombes de coups **\n");
        }
        while (premierLancement == 1)// Si c'est le premier lancement et que l'option est activ� affichage des regles
        {
            premierLancement = affichageRegles(premierLancement);
        }
    //Stats + Niveau etc
    printf("\n===Statistiques===\n\n");
    printf(" Difficulte : ");
    if (difficulte == 1)
        printf("Facile\n");
    if (difficulte == 2)
        printf("Normal\n");
    if (difficulte == 3)
        printf("Difficile\n");
    if (difficulte == 4)
        printf("Impossible\n");
    printf(" Mode Hardcore : ");
    if (hardcoreMode == 0)
        printf("Desactive\n\n");
    if (hardcoreMode == 1)
        printf("Active\n\n");
    printf(" Nombre de coups restants: %d\n", coupsRestants);
    printf(" Niveau : %d\n", niveau);
    printf(" Or : %d\n\n", quantiteOr);
    printf(" Essaie realiser: %d\n", essaieRealiser);
    printf(" %d Victoire(s) / %d Defaite(s)\n\n", victoire, defaite);


    //Message de bienvenue et de choix.
    printf("=== Menu === \n\nBonjour jeune aventurier, bienvenue dans mon petit jeu.\n\n");
    printf("Que veux-tu faire ?\n\n");
    printf(" 1. Jouer contre la chance elle meme ! (Niveau %d)\n", niveau+1);
    printf(" 2. Jouer contre un ami. (Mode Versus)\n");
    printf(" 3. Ameliorer mon nombre de coups restants(Cout: %d d'or).\n", coutAmeliorationCoups);
    printf(" 4. Ameliorer mon gain d'or (Cout: %d d'or).\n", coutAmeliorationOr);
    printf(" 5. Changer le niveau de difficulte.\n");
    printf(" 6. Activer/Desactiver le mode hardcore.\n");
    printf(" 7. Rappel moi les regles du jeu !\n");
    printf(" 8. Je veux quitter le jeu.\n");

    scanf("%d", &choixMenu);
        //Gros menu en forme de pavet
    switch (choixMenu)
    {
    case 1:
        system("cls");
        coupsRestants = jeuSolo(nombreSRAND, coupsRestants);
        if (coupsRestants == 0)
        {
            system("cls");
            defaite++, essaieRealiser++;
            quantiteOr= quantiteOr + 5 + ameliorationOr*10;
            printf("\n Tu as perdu. Essaie encore, tu auras peut-etre plus de chance !\n");
        }
        else if (coupsRestants != 0)
        {
            victoire++, essaieRealiser++;
            niveau++;
            quantiteOr= quantiteOr + coupsRestants*5 + ameliorationOr*10;
        }
        break;

    case 2:
        system("cls");
        coupsRestants = jeuMulti(coupsRestants);
        if (coupsRestants == 0)
        {
            system("cls");
            printf("\n Tu as perdu. Essaie encore, tu auras peut-etre plus de chance !\n");
        }
        break;

    case 3:
        system("cls");
        if (quantiteOr >= coutAmeliorationCoups)
        {
            ameliorationCoups++;
            coups++;
            quantiteOr = quantiteOr - coutAmeliorationCoups;
            coutAmeliorationCoups = coutAmeliorationCoups + 10;
            printf("Amelioration reussi avec succes.\n\n");
        }
        else
            printf("Tu n'as pas assez d'or pour ameliorer ton nombre de coups. Retourne jouer.\n\n");
        break;

    case 4:
        system("cls");
        if (quantiteOr >= coutAmeliorationOr)
        {
            ameliorationOr++;
            quantiteOr = quantiteOr - coutAmeliorationOr;
            coutAmeliorationOr = coutAmeliorationOr + 10;
            printf("Amelioration reussi avec succes.\n\n");
        }
        else
            printf("Tu n'as pas assez d'or pour ameliorer ton revenue d'or. Retourne jouer.\n\n");
        break;

    case 5: // Changer le niveau de difficulte
        difficulte = swiftDifficulty(difficulte);
        break;

    case 6:
        if (hardcoreMode == 0 )
        {
            system("cls");
            hardcoreMode++;
            printf("\n Le mode hardcore a ete correctement active.\n\n");
        }
        else if(hardcoreMode == 1)
        {
            system("cls");
            hardcoreMode--;
            printf("\n Le mode hardcore a ete correctement desactive.\n\n");
        }
        break;

    case 7:
        system("cls");
        premierLancement = 1;
        break;

    case 8:
        system("cls");
        etatDuJeu--;
        printf("Es-tu sur de toi ?\n Toute progression sera perdu(je sais pas encore faire de sauvegarde).\n");
        printf(" 1. Oui, je veux quitter.\n");
        printf(" 2. Non j'ai missclick ou misstap en l'occurence.\n");
        printf("Toute autre reponse sera consideree comme un non.\n");
        scanf("%d",&choixConfirmation);
        switch (choixConfirmation)
            {
            case 1:
                system("cls");
                etatDuJeu--;
                break;
            default:
                system("cls");
                etatDuJeu = 2;
                break;
            }
        break;

    case 1001:
        system("cls");
        printf(" ** Attention les erreurs ne sont pas gerer pour la modificcation des variables ! **\n\n");
        printf("\n Quel niveau veux-tu etre ?\n\n");
        scanf("%d", &niveau);
        system("cls");
        printf("\n Nouveau niveau definie avec succes.\n\n");
        break;

    case 1002:
        system("cls");
        printf(" ** Attention les erreurs ne sont pas gerer pour la modificcation des variables ! **\n\n");
        printf("\n Quelle est la quantitee d'or souhaiter ?\n\n");
        scanf("%d", &quantiteOr);
        system("cls");
        printf("\n Quantiter d'or change avec succes.\n\n");
        break;

    case 1003:
        system("cls");
        printf(" ** Attention les erreurs ne sont pas gerer pour la modificcation des variables ! **\n\n");
        printf("\n Combien de coups restants veux tu avoir ?\n\n");
        scanf("%d", &coupsRestants);
        system("cls");
        printf("\n Nombre de coups restants changer avec succes.\n\n");
        break;

    case 3357: /* Active/Desactive la "console" de debug avec visu des stats, etc.. */
        system("cls");
        if (debugMode == 45)
            debugMode = 2;
        else
            debugMode = 45;
        break;
    default:
        system("cls");
        printf("Tu ne peux choisir que entre le menu 1, 2, 3, 4 , 5, 6, 7 ou 8. Reessaye.\n");
        break;
    }
    if (hardcoreMode == 1 && coupsRestants == 0)
        {
            reset = 1;
            etatDuJeu = 0;
        }
    }
    if (reset == 1)
        printf(" Le mode hardcore etait active, ta progression est donc retomber a zero.\n ");
 }while (reset != 0);

 return 0;
}
int jeuMulti(int coups)
{
    int compteurCoups = 0, nombreEntre = 0, nombreSRAND = 0;

    system("cls");
    printf("\n Quel est le nombre a faire deviner ?\n\n");
    scanf("%d", &nombreSRAND);
    printf("\n En combien de coup(s) diot-il etre deviner ?\n\n");
    scanf("%d", &coups);
    system("cls");
    do
        {
            compteurCoups++;
            printf("Quel est le nombre ?\n");
            scanf("%d", &nombreEntre);
            if (nombreSRAND > nombreEntre)
            {
                coups --;
                printf("C'est plus !");
                if (coups < 1000)
                    printf(" Coups restant %d.", coups);
                printf("\n\n");
            }
            else if (nombreSRAND < nombreEntre)
            {
                coups --;
                printf("C'est moins !");
                if (coups < 1000)
                    printf(" Coups restant %d.", coups);
                printf("\n\n");
            }
            else
                {
                system("cls");
                printf ("\n Bravo, tu as trouve le nombre mystere en %d coup(s).\n", compteurCoups);
                printf(" Le nombre mystere etait le %d.\n", nombreSRAND);
                }
        } while (nombreEntre != nombreSRAND && coups != 0);
        if (coups == 0)
            {
                system("cls");
                printf("\n Tu as perdu. Essaie encore, tu auras peut-etre plus de chance !\n");
                printf(" Le nombre mystere etait le %d.\n", nombreSRAND);
            }
    return (coups);
}

int jeuSolo(int nombreSRAND, int coups)
{
    int compteurCoups = 0, nombreEntre = 0;

    do
        {
            compteurCoups++;
            printf("Quel est le nombre ?\n");
            scanf("%d", &nombreEntre);
            if (nombreSRAND > nombreEntre)
            {
                coups --;
                printf("C'est plus !");
                if (coups < 1000)
                    printf(" Coups restant %d.", coups);
                printf("\n\n");
            }
            else if (nombreSRAND < nombreEntre)
            {
                coups --;
                printf("C'est moins !");
                if (coups < 1000)
                    printf(" Coups restant %d.", coups);
                printf("\n\n");
            }
            else
                {
                system("cls");
                printf ("\n Bravo, tu as trouve le nombre mystere en %d coup(s).\n", compteurCoups);
                printf(" Le nombre mystere etait le %d.\n", nombreSRAND);
                }
        } while (nombreEntre != nombreSRAND && coups != 0);
        if (coups == 0)
            {
                system("cls");
                printf("\n Tu as perdu. Essaie encore, tu auras peut-etre plus de chance !\n");
                printf(" Le nombre mystere etait le %d.\n", nombreSRAND);
            }
    return (coups);
}

int affichageRegles(int premierLancement)
{
    int choix = 0;

    printf("\n===Regles===\n\n");
    printf("Le but du jeu est de trouver le nombre mystere en faisant le moins de coups possible.\n");
    printf("Il existe quatre mode de difficulte: facile, normal, difficile et impossible.\n");
    printf("Le nombre mystere depend de la difficulte et il augmente en fonction du  niveau atteint.\n");
    printf("Pour passer un niveau il te fauttrouver le nombre mystere sans utilise tout tes coups.\n");
    printf("Chaque fois que tu joue (en solo), tu gagne de l'or. Chaque fois que tu passe un niveau aussi.\n");
    printf("Tu peux ameliorer ton nombre de coups et la quantite d'or gagner. \n");
    printf("Plus tu ameliore ton nombre de coups plus tu auras de chance de passer au  niveau suivant, plus tu gagneras d'or.\n");
    printf("Il y a un mode hardcore, si tu perds quand ce dernier est active, toute la progression est reinitialisee.\n");
    printf("Voila pour les regles du jeu, elle sont simple mais efficace. \n\n As-tu bien compris ?\n");
    printf(" 1. Oui.\n 2. Non.\n");
    scanf("%d", &choix);
    switch (choix)
    {
        case 1:
            system("cls");
            premierLancement = 0;
            break;
        case 2:
            system("cls");
            premierLancement = 1;
            break;
        default:
            system("cls");
            premierLancement = 1;
            printf("Tu ne peux choisir que 1 (Oui) ou 2 (Non).\nEssaye encore.\n\n");
            break;

    }
    return (premierLancement);
}


int swiftDifficulty(int difficulte)
{
    int choixAutre = 0;

    system("cls");
        printf("\n ~~ Work In Progress ~~ \n\n");
        printf(" Le niveau de difficulte actuel est ");
        if (difficulte == 1)
            printf("facile.\n");
        if (difficulte == 2)
            printf("normal.\n");
        if (difficulte == 3)
            printf("difficile.\n");
        if (difficulte == 4)
            printf("impossible.\n");
        printf("\n Quel est le niveau de difficulte que tu souhaite ?\n\n");
        printf(" 1. Facile.\n");
        printf(" 2. Normale.\n");
        printf(" 3. Difficile.\n");
        printf(" 4. Impossible.\n");
        scanf("%d", &choixAutre);
            switch (choixAutre) // Choix du niveau de difficultee
            {
                case 1:
                    system("cls");
                    difficulte = 1;
                    printf(" La difficulte facile a ete apliquer avec succes\n\n");
                    break;
                case 2:
                    system("cls");
                    difficulte = 2;
                    printf(" La difficulte normale a ete apliquer avec succes\n\n");
                    break;
                case 3:
                    system("cls");
                    difficulte = 3;
                    printf(" La difficulte difficile a ete apliquer avec succes\n\n");
                    break;
                case 4:
                    system("cls");
                    difficulte = 4;
                    printf(" La difficulte impossible a ete apliquer avec succes\n\n");
                    break;
                default:
                    system("cls");
                    printf("Mauvaise reponse, retrour au menu. Essaye avec 1, 2, 3 ou 4.\n\n");
                    break;
            }
            return (difficulte);
}
